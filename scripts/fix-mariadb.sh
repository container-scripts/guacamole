#!/bin/bash

source /host/settings.sh

# fix a bug with systemd
# see: https://jira.mariadb.org/browse/MDEV-23050
if [[ -z $DBHOST ]]; then
    sed -i /lib/systemd/system/mariadb.service \
        -e '/SendSIGKILL/ c SendSIGKILL=yes'
    systemctl daemon-reload
    systemctl restart mariadb
fi
