#!/bin/bash -x

# apache2 small ram configuration
cat <<EOF > /etc/apache2/mods-available/mpm_event.conf
<IfModule mpm_event_module>
    StartServers              1
    MinSpareThreads          13
    MaxSpareThreads          45
    ThreadLimit              32
    ThreadsPerChild          13
    MaxRequestWorkers        80
    MaxConnectionsPerChild    0
</IfModule>
EOF
systemctl restart apache2

# tomcat
sed -i /etc/default/tomcat9 \
    -e '/^JAVA_OPTS/ a JAVA_OPTS="${JAVA_OPTS} -Xmx20m -Xms20m"'
systemctl restart tomcat9
