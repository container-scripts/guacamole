#!/bin/bash -x

# based on: https://guacamole.apache.org/doc/gug/proxying-guacamole.html#apache
cat <<EOF > /etc/apache2/conf-available/guacamole.conf
ProxyPass /chat !

ProxyPass / http://localhost:8080/guacamole/ flushpackets=on
ProxyPassReverse / http://localhost:8080/guacamole/
ProxyPassReverseCookiePath /guacamole/ /

<Location /websocket-tunnel>
    Require all granted
    ProxyPass ws://localhost:8080/guacamole/websocket-tunnel
    ProxyPassReverse ws://localhost:8080/guacamole/websocket-tunnel
</Location>

SetEnvIf Request_URI "^/tunnel" dontlog
CustomLog  /var/log/apache2/guacamole.log common env=!dontlog
EOF

# comment CustomLog on 'default-ssl.conf'
sed -i /etc/apache2/sites-available/default-ssl.conf \
    -e 's/CustomLog/#CustomLog/'

a2enmod ssl proxy proxy_http proxy_wstunnel
a2ensite default-ssl.conf
a2enconf guacamole.conf

systemctl reload apache2
