#!/bin/bash -x

source /host/settings.sh

# if DB settings are not defined, use some default values
DBHOST=${DBHOST:-localhost}
DBPORT=${DBPORT:-3306}
DBNAME=${DBNAME:-guacamole_db}
DBUSER=${DBUSER:-guacamole_user}
DBPASS=${DBPASS:-$(tr -cd '[:alnum:]' < /dev/urandom | fold -w16 | head -n1)}

if [[ $DBHOST == 'localhost' ]]; then
    # create the database and user
    mariadb -e "
        DROP DATABASE IF EXISTS $DBNAME;
        CREATE DATABASE $DBNAME;
        DROP USER IF EXISTS '$DBUSER'@'localhost';
        CREATE USER '$DBUSER'@'localhost' IDENTIFIED BY '$DBPASS';
        GRANT ALL PRIVILEGES ON $DBNAME.* TO '$DBUSER'@'localhost';
        FLUSH PRIVILEGES;
    "
else
    systemctl stop mariadb
    systemctl disable mariadb
fi

# set the options of the mariadb command
mariadb="mariadb -h $DBHOST -P $DBPORT -D $DBNAME -u $DBUSER --password=$DBPASS"

# create the db tables
cat /etc/guacamole/001-create-schema.sql | $mariadb

# create an admin user, if it does not exist
nr=$($mariadb -sN -e 'SELECT COUNT(*) FROM guacamole_entity')
if [[ $nr == '0' ]]; then
    cat /etc/guacamole/002-create-admin-user.sql | $mariadb
fi

# set the username and password of the admin user to $ADMIN and $PASS
[[ -n $PASS ]] && $mariadb -e "
    SET @salt = UNHEX(SHA2(UUID(), 256));

    UPDATE guacamole_user
    SET password_salt = @salt,
        password_hash = UNHEX(SHA2(CONCAT('$PASS', HEX(@salt)), 256))
    WHERE user_id = 1;

    UPDATE guacamole_entity
    SET name = '$ADMIN'
    WHERE entity_id = 1 AND type = 'USER';
"

# cleanup the mariadb init scripts
rm /etc/guacamole/00*.sql

# setup guacamole.properties
mkdir -p /etc/guacamole
cat <<EOF > /etc/guacamole/guacamole.properties
guacd-hostname: localhost
guacd-port:     4822

#mysql-driver:   mariadb
mysql-hostname: $DBHOST
mysql-port:     $DBPORT
mysql-database: $DBNAME
mysql-username: $DBUSER
mysql-password: $DBPASS
mysql-default-max-connections: ${MAX_CONNECTIONS:-50}
mysql-default-max-connections-per-user: ${MAX_CONNECTIONS:-50}
EOF

# link to tomcat libraries
ln -sf /etc/guacamole/guacamole.properties \
   /usr/share/tomcat9/lib/guacamole.properties

systemctl restart tomcat9
