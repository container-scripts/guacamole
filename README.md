# Guacamole In a Container

## Installation

- First install `cs` and `wsproxy`:
   - https://gitlab.com/container-scripts/cs#installation
   - https://gitlab.com/container-scripts/wsproxy#installation

- Then get the scripts: `cs pull guacamole`

- Create a dir for the container: `cs init guacamole @guac.example.org`

- Fix the settings: `cd /var/cs/guac.example.org/; vim settings.sh`
  Make sure to change the admin password.

- Make the container: `cs make`

- Open https://guac.example.org/

See also this interactive tutorial:
https://katacoda.com/dashohoxha/courses/container-scripts/virtual-computer-lab

## Configuration

First you need to install one or more Linux servers:
  - https://gitlab.com/container-scripts/linuxmint#installation 

Then you can create users and connections like this:
```bash
cs guac help
cs guac server add linuxmint-1
cs guac user add username p@ssw0rd
cs guac user connect username linuxmint-1
```

In this case `linuxmint-1` is the name of the container where the
Linux server is installed, or its domain, or its IP.

## Using an external DB

If these lines on `settings.sh` are uncommented, guacamole will use an
external DB:

```bash
DBHOST=mariadb    # this is the name of the mariadb container
DBPORT=3306
DBNAME=guac_example_org
DBUSER=guac_example_org
DBPASS=123456
```

Using an external DB has the advantage that the configurations persist
when you remake the container.

## Accessing in a LAN

If the guacamole server is in a LAN (for example in a laptop) it has to
be accessed by its IP (like this: https://123.45.67.89/). In this case
the `wsproxy` container is not useful (since it is based on apache2
virtual domains). To access the guacamole server in this case you have
to uncomment the line `PORTS="443:433"` on `settings.sh` (and maybe
forward another port if needed, like `PORTS="444:443"`). This has to
be done before `cs make`.

Also, make sure to comment out `DOMAIN` and `SSL_CERT_EMAIL`.

## Accessing in a Wi-Fi LAN

If the container is in a laptop and there is no wi-fi access point
around to enable the connection/communication with other laptops,
we can start a hotspot like this:
```
cs hotspot start test-hotspot pass1234
```

Afterwards the server can be accessed on https://10.42.0.1/

**Note:** Depending on the type of the wireless card or the driver
that it has, the hotspot may not always work. In this case the most
easy way to create a hotspot is with an Android or iPhone.
