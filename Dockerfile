include(focal)

### update and upgrade
RUN apt update &&\
    apt upgrade --yes

RUN DEBIAN_FRONTEND=noninteractive \
    apt install --yes tomcat9 apache2 wget

### install Guacamole dependences
RUN apt install --yes \
        build-essential libcairo2-dev libjpeg-turbo8-dev libpng-dev libtool-bin libossp-uuid-dev
RUN apt install --yes \
        libavcodec-dev libavformat-dev libavutil-dev libswscale-dev \
        freerdp2-dev libpango1.0-dev libssh2-1-dev libtelnet-dev libvncserver-dev libwebsockets-dev \
        libpulse-dev libssl-dev libvorbis-dev libwebp-dev

ENV VER=1.2.0
ENV ARCHIVE=http://archive.apache.org/dist/guacamole/$VER

### install guacamole server
RUN wget "$ARCHIVE/source/guacamole-server-$VER.tar.gz" && \
    tar -xzf guacamole-server-$VER.tar.gz && \
    cd guacamole-server-*/ && \
    ./configure --with-init-dir=/etc/init.d && \
    make && \
    make install && \
    ldconfig && \
    systemctl enable guacd && \
    cd .. && \
    rm -rf guacamole-server-$VER/ && \
    rm guacamole-server-$VER.tar.gz

### install guacamole client
RUN wget "$ARCHIVE/binary/guacamole-$VER.war" && \
    mkdir -p /etc/guacamole/ && \
    mv guacamole-$VER.war /etc/guacamole/guacamole.war && \
    ln -sf /etc/guacamole/guacamole.war /var/lib/tomcat9/webapps/

### install mariadb
RUN apt install --yes mariadb-server mariadb-client mariadb-common libmariadb-java &&\
    mkdir -p /etc/guacamole/lib && \
    ln -sf /usr/share/java/mariadb-java-client.jar /etc/guacamole/lib/

### install mysql-connector-java
### there is a problem related to mariadb driver
### see: https://mail-archives.apache.org/mod_mbox/guacamole-user/202007.mbox/%3CCAMucfLyRUocc4Z%3DiO45zmnMZ6K7%2BLYFrXiH1D_QQA7CjtoqhPA%40mail.gmail.com%3E
RUN wget https://repo.mysql.com/apt/ubuntu/pool/mysql-tools/m/mysql-connector-java/mysql-connector-java_8.0.20-1ubuntu20.04_all.deb && \
    apt install -f --yes ./mysql-connector-java*.deb &&\
    mkdir -p /etc/guacamole/lib &&\
    ln -sf /usr/share/java/mysql-connector-java*.jar /etc/guacamole/lib/mysql-connector-java.jar

### install the auth-jdbc extension
RUN wget "$ARCHIVE/binary/guacamole-auth-jdbc-$VER.tar.gz" && \
    tar -xzf guacamole-auth-jdbc-$VER.tar.gz && \
    mkdir -p /etc/guacamole/extensions/ && \
    cp guacamole-auth-jdbc-$VER/mysql/guacamole-auth-jdbc-mysql-$VER.jar /etc/guacamole/extensions/ && \
    cp guacamole-auth-jdbc-$VER/mysql/schema/*.sql /etc/guacamole/ && \
    rm guacamole-auth-jdbc-$VER.tar.gz && \
    rm -rf guacamole-auth-jdbc-$VER/

### install font Monaco
RUN mkdir -p /usr/share/fonts/truetype/Monaco && \
    cd /usr/share/fonts/truetype/Monaco && \
    wget https://github.com/mikesmullin/gvim/raw/master/.fonts/Monaco_Linux.ttf && \
    fc-cache -f .

RUN DEBIAN_FRONTEND=noninteractive \
    apt install --yes php
