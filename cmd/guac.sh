cmd_guac_help() {
    cat <<_EOF
    guac [<command>] [<arguments>]
        Simple command to manage guacamole servers, users and connections between them.
        It can be used like this:

        - server add <server>
        - server (list | ls)
        - server (remove | rm) <server>

        - user add <username> <password>
        - user (list | ls) [<username>]
        - user (remove | rm) <username>
        - user connect <username> <server>
        - user disconnect <username> <server>

        - guest add <server> <username> [<password>]
        - guest (remove | rm) <server> [<username>]
        - guest (list | ls) [<server>]

_EOF
}

cmd_guac() {
    local cmd=$1
    shift
    case $cmd in
        server)
            _guac_server "$@" ;;
        user)
            _guac_user "$@" ;;
        guest)
            _guac_guest "$@" ;;
        *)
            _guac_fail ;;
    esac
}

_guac_fail() {
    fail "Usage:\n$(cmd_guac_help)"
}

_mariadb() {
    local mariadb="mariadb guacamole_db"
    [[ -n $DBHOST ]] && mariadb="mariadb -h $DBHOST -P $DBPORT -D $DBNAME -u $DBUSER --password=$DBPASS"
    echo -e "$*" | podman exec -i $CONTAINER $mariadb -sN
}

_guac_server() {
    local cmd=$1
    shift
    case $cmd in
        add)
            _guac_server_add "$@" ;;
        list|ls)
            _guac_server_list "$@" ;;
        remove|rm)
            _guac_server_remove "$@" ;;
        *)
            _guac_fail ;;
    esac
}

_guac_user() {
    local cmd=$1
    shift
    case $cmd in
        add)
            _guac_user_add "$@" ;;
        list|ls)
            _guac_user_list "$@" ;;
        remove|rm)
            _guac_user_remove "$@" ;;
        connect)
            _guac_user_connect "$@" ;;
        disconnect)
            _guac_user_disconnect "$@" ;;
        *)
            _guac_fail ;;
    esac
}

_guac_guest() {
    local cmd=$1
    shift
    case $cmd in
        add)
            _guac_guest_add "$@" ;;
        list|ls)
            _guac_guest_list "$@" ;;
        remove|rm)
            _guac_guest_remove "$@" ;;
        *)
            _guac_fail ;;
    esac
}

_guac_server_add() {
    local server=$1
    [[ -z $server ]] && _guac_fail

    # check that it does not already exist
    local id=$(_mariadb "
        SELECT connection_id FROM guacamole_connection
        WHERE connection_name = '$server' AND protocol = 'rdp';
    ")
    [[ -n $id ]] && fail "Server already exists"

    # create guacamole connections for RDP and SSH
    _mariadb "
        INSERT INTO guacamole_connection
            (connection_name, protocol, max_connections_per_user)
        VALUES
            ('$server', 'rdp', NULL),
            ('$server', 'ssh', NULL),
            ('${server}-guest', 'rdp', 1);
    "

    # set some parameters for these connections
    local id_rdp=$(_mariadb "
        SELECT connection_id FROM guacamole_connection
        WHERE connection_name = '$server' AND protocol = 'rdp';
    ")
    local id_ssh=$(_mariadb "
        SELECT connection_id FROM guacamole_connection
        WHERE connection_name = '$server' AND protocol = 'ssh';
    ")
    local id_guest=$(_mariadb "
        SELECT connection_id FROM guacamole_connection
        WHERE connection_name = '${server}-guest' AND protocol = 'rdp';
    ")
    _mariadb "
        INSERT INTO guacamole_connection_parameter
        VALUES
            ($id_rdp, 'hostname', '$server'),
            ($id_ssh, 'hostname', '$server'),
            ($id_ssh, 'font-name', 'Monaco'),
            ($id_guest, 'hostname', '$server'),
            ($id_guest, 'username', '\${GUAC_USERNAME}'),
            ($id_guest, 'password', '\${GUAC_PASSWORD}');
    "

    # create sharing profiles Watch and Collaborate for these connections
    _mariadb "
        INSERT INTO guacamole_sharing_profile
            (sharing_profile_name, primary_connection_id)
        VALUES
            ('Watch', $id_rdp),
            ('Collaborate', $id_rdp),
            ('Watch', $id_ssh),
            ('Collaborate', $id_ssh),
            ('Watch', $id_guest),
            ('Collaborate', $id_guest);
    "

    # add the parameter read-only for the Watch sharing profiles
    local id_watch_rdp=$(_mariadb "
        SELECT sharing_profile_id FROM guacamole_sharing_profile
        WHERE primary_connection_id = '$id_rdp' AND sharing_profile_name = 'Watch';
    ")
    local id_watch_ssh=$(_mariadb "
        SELECT sharing_profile_id FROM guacamole_sharing_profile
        WHERE primary_connection_id = '$id_ssh' AND sharing_profile_name = 'Watch';
    ")
    local id_watch_guest=$(_mariadb "
        SELECT sharing_profile_id FROM guacamole_sharing_profile
        WHERE primary_connection_id = '$id_guest' AND sharing_profile_name = 'Watch';
    ")
    _mariadb "
        INSERT INTO guacamole_sharing_profile_parameter
        VALUES
            ($id_watch_rdp, 'read-only', 'true'),
            ($id_watch_ssh, 'read-only', 'true'),
            ($id_watch_guest, 'read-only', 'true');
    "
}

_guac_server_list() {
    _mariadb "
        SELECT connection_name FROM guacamole_connection
        WHERE protocol = 'ssh'
    "
}

_guac_server_remove() {
    local server=$1
    [[ -z $server ]] && _guac_fail

    _mariadb "
        DELETE FROM guacamole_connection
        WHERE connection_name='$server';

        DELETE FROM guacamole_connection
        WHERE connection_name='${server}-guest';
    "

    # parameters and sharing profiles will be deleted automatically by the database triggers
}

_guac_user_add() {
    local username=$1
    [[ -z $username ]] && _guac_fail
    local password=$2
    [[ -z $password ]] && _guac_fail

    # check that it does not already exist
    local id=$(_mariadb "
        SELECT entity_id FROM guacamole_entity
        WHERE name = '$username' AND type = 'USER'
    ")
    [[ -n $id ]] && fail "User already exists"

    # create a user entity
    _mariadb "
        INSERT INTO guacamole_entity
        SET type = 'USER',
            name = '$username';
    "

    # insert the user
    local entity_id=$(_mariadb "
        SELECT entity_id FROM guacamole_entity 
        WHERE name = '$username' AND type = 'USER'
    ")
    _mariadb "
        SET @salt = UNHEX(SHA2(UUID(), 256));
        INSERT INTO guacamole_user
        SET entity_id = $entity_id,
            password_hash = UNHEX(SHA2(CONCAT('$password', HEX(@salt)), 256)),
            password_salt = @salt,
            password_date = NOW();
    "

    # set permission for this user to READ
    local user_id=$(_mariadb "
        SELECT user_id FROM guacamole_user 
        WHERE entity_id = '$entity_id'
    ")
    _mariadb "
        INSERT INTO guacamole_user_permission
        VALUES
            ($entity_id,$user_id,'READ');
    "    
}

_guac_user_list() {
    local username=$1

    if [[ -z $username ]]; then
        _mariadb "
            SELECT name
            FROM guacamole_entity
            NATURAL JOIN guacamole_user
            WHERE type = 'USER'
              AND name != '$ADMIN'
              AND full_name IS NULL;
        "
    else
        _mariadb "
            SELECT connection_name
            FROM guacamole_entity
            NATURAL JOIN guacamole_connection_permission
            NATURAL JOIN guacamole_connection
            WHERE name = '$username'
            AND protocol = 'rdp'
        "
    fi
}

_guac_user_remove() {
    local username=$1
    [[ -z $username ]] && _guac_fail
    [[ $username == $ADMIN ]] && return

    _mariadb "
        DELETE FROM guacamole_entity
        WHERE name = '$username' AND type = 'USER';
    "

    # all the dependent records will be deleted automatically by the database
}

_guac_user_connect() {
    local username=$1
    [[ -z $username ]] && _guac_fail
    local server=$2
    [[ -z $server ]] && _guac_fail

    # get the user id (or entity_id)
    local uid=$(_mariadb "
        SELECT entity_id FROM guacamole_entity 
        WHERE name = '$username' AND type = 'USER';
    ")

    # get connection ids for the rdp and ssh connections
    local rdp_cid=$(_mariadb "
        SELECT connection_id FROM guacamole_connection
        WHERE connection_name = '$server' AND protocol = 'rdp';
    ")
    local ssh_cid=$(_mariadb "
        SELECT connection_id FROM guacamole_connection
        WHERE connection_name = '$server' AND protocol = 'ssh';
    ")

    # get the sharing profile ids for each connection
    local rdp_watch_pid=$(_mariadb "
        SELECT sharing_profile_id FROM guacamole_sharing_profile
        WHERE primary_connection_id = $rdp_cid
          AND sharing_profile_name = 'Watch';
    ")
    local rdp_collab_pid=$(_mariadb "
        SELECT sharing_profile_id FROM guacamole_sharing_profile
        WHERE primary_connection_id = $rdp_cid
          AND sharing_profile_name = 'Collaborate';
    ")
    local ssh_watch_pid=$(_mariadb "
        SELECT sharing_profile_id FROM guacamole_sharing_profile
        WHERE primary_connection_id = $ssh_cid
          AND sharing_profile_name = 'Watch';
    ")
    local ssh_collab_pid=$(_mariadb "
        SELECT sharing_profile_id FROM guacamole_sharing_profile
        WHERE primary_connection_id = $ssh_cid
          AND sharing_profile_name = 'Collaborate';
    ")

    # set connection permissions and sharing profile permissions to READ
    _mariadb "
        INSERT INTO guacamole_connection_permission
        VALUES
            ($uid,$rdp_cid,'READ'),
            ($uid,$ssh_cid,'READ');

        INSERT INTO guacamole_sharing_profile_permission
        VALUES
            ($uid,$rdp_watch_pid,'READ'),
            ($uid,$rdp_collab_pid,'READ'),
            ($uid,$ssh_watch_pid,'READ'),
            ($uid,$ssh_collab_pid,'READ');
    "
}

_guac_user_disconnect() {
    local username=$1
    [[ -z $username ]] && _guac_fail
    local server=$2
    [[ -z $server ]] && _guac_fail

    # get the user id (or entity_id)
    local uid=$(_mariadb "
        SELECT entity_id FROM guacamole_entity
        WHERE name = '$username' AND type = 'USER';
    ")

    # get connection ids for the rdp and ssh connections
    local rdp_cid=$(_mariadb "
        SELECT connection_id FROM guacamole_connection
        WHERE connection_name = '$server' AND protocol = 'rdp';
    ")
    local ssh_cid=$(_mariadb "
        SELECT connection_id FROM guacamole_connection
        WHERE connection_name = '$server' AND protocol = 'ssh';
    ")

    # get the sharing profile ids for each connection
    local rdp_watch_pid=$(_mariadb "
        SELECT sharing_profile_id FROM guacamole_sharing_profile
        WHERE primary_connection_id = $rdp_cid
          AND sharing_profile_name = 'Watch';
    ")
    local rdp_collab_pid=$(_mariadb "
        SELECT sharing_profile_id FROM guacamole_sharing_profile
        WHERE primary_connection_id = $rdp_cid
          AND sharing_profile_name = 'Collaborate';
    ")
    local ssh_watch_pid=$(_mariadb "
        SELECT sharing_profile_id FROM guacamole_sharing_profile
        WHERE primary_connection_id = $ssh_cid
          AND sharing_profile_name = 'Watch';
    ")
    local ssh_collab_pid=$(_mariadb "
        SELECT sharing_profile_id FROM guacamole_sharing_profile
        WHERE primary_connection_id = $ssh_cid
          AND sharing_profile_name = 'Collaborate';
    ")

    # delete connection permissions and sharing profile permissions
    _mariadb "
        DELETE FROM guacamole_connection_permission
        WHERE entity_id = $uid AND connection_id = $rdp_cid;

        DELETE FROM guacamole_connection_permission
        WHERE entity_id = $uid AND connection_id = $ssh_cid;

        DELETE FROM guacamole_sharing_profile_permission
        WHERE entity_id = $uid AND sharing_profile_id = $rdp_watch_pid;

        DELETE FROM guacamole_sharing_profile_permission
        WHERE entity_id = $uid AND sharing_profile_id = $rdp_collab_pid;

        DELETE FROM guacamole_sharing_profile_permission
        WHERE entity_id = $uid AND sharing_profile_id = $ssh_watch_pid;

        DELETE FROM guacamole_sharing_profile_permission
        WHERE entity_id = $uid AND sharing_profile_id = $ssh_collab_pid;
    "
}

_guac_guest_add() {
    local server=$1
    [[ -z $server ]] && _guac_fail
    local username=$2
    [[ -z $username ]] && _guac_fail
    local password=$3
    [[ -z $password ]] && password=$username

    # add user
    _guac_user_add $username $password
    
    # set full_name to 'Guest'
    # this is how we distinguish a guest user from a normal user
    local uid=$(_mariadb "
        SELECT entity_id FROM guacamole_entity 
        WHERE name = '$username' AND type = 'USER'
    ")
    _mariadb "
        UPDATE guacamole_user
        SET full_name = 'Guest'
        WHERE entity_id = $uid;
    "

    # get connection ids for the connection and sharing profiles
    local rdp_cid=$(_mariadb "
        SELECT connection_id FROM guacamole_connection
        WHERE connection_name = '${server}-guest' AND protocol = 'rdp';
    ")
    local rdp_watch_pid=$(_mariadb "
        SELECT sharing_profile_id FROM guacamole_sharing_profile
        WHERE primary_connection_id = $rdp_cid
          AND sharing_profile_name = 'Watch';
    ")
    local rdp_collab_pid=$(_mariadb "
        SELECT sharing_profile_id FROM guacamole_sharing_profile
        WHERE primary_connection_id = $rdp_cid
          AND sharing_profile_name = 'Collaborate';
    ")

    # set connection permissions and sharing profile permissions to READ
    _mariadb "
        INSERT INTO guacamole_connection_permission
        VALUES
            ($uid,$rdp_cid,'READ');

        INSERT INTO guacamole_sharing_profile_permission
        VALUES
            ($uid,$rdp_watch_pid,'READ'),
            ($uid,$rdp_collab_pid,'READ');
    "
}

_guac_guest_remove() {
    local server=$1
    [[ -z $server ]] && _guac_fail
    local username=$2

    local match_username=''
    [[ -n $username ]] && match_username=" AND name = '$username'"
    _mariadb "
        CREATE TEMPORARY TABLE remove_list AS
            SELECT entity_id
            FROM guacamole_connection_permission
            NATURAL JOIN guacamole_connection_parameter
            NATURAL JOIN guacamole_entity
            NATURAL JOIN guacamole_user
            WHERE parameter_name = 'hostname'
              AND parameter_value = '$server'
              AND type = 'USER'
              AND full_name = 'Guest'
              $match_username
              ;

        DELETE FROM guacamole_entity
        WHERE entity_id IN 
              (SELECT entity_id FROM remove_list);
    "
    
    # all the dependent records will be deleted automatically by the database
}

_guac_guest_list() {
    _mariadb "
        SELECT parameter_value, name
        FROM guacamole_entity
        NATURAL JOIN guacamole_user
        NATURAL JOIN guacamole_connection_permission
        NATURAL JOIN guacamole_connection
        NATURAL JOIN guacamole_connection_parameter
        WHERE name != '$ADMIN'
          AND type = 'USER'
          AND full_name = 'Guest'
          AND parameter_name = 'hostname';
    "
}
