cmd_restore_help() {
    cat <<_EOF
    restore <backup-file.tgz>
        Restore from the given backup file.

_EOF
}

cmd_restore() {
    local file=$1
    [[ ! -f $file ]] && fail "Usage:\n$(cmd_restore_help)"
    local backup=${file%%.tgz}
    backup=$(basename $backup)

    # extract the backup archive
    tar --extract --gunzip --preserve-permissions --file=$file

    # restore the content of the database
    local mariadb="podman exec -i $CONTAINER mariadb"
    if [[ -z $DBHOST ]]; then
        cat $backup/guacamole_db.sql | $mariadb guacamole_db
    else
        mariadb+=" -h $DBHOST -P $DBPORT -D $DBNAME -u $DBUSER --password=$DBPASS"
        cat $backup/$DBNAME.sql | $mariadb
    fi

    # clean up
    rm -rf $backup
}
