cmd_hotspot_help() {
    cat <<_EOF
    hotspot [ start [<ssid>] [<pass>] | stop ]
	If no <ssid> is given, the hostname will be used by default.
	The default <pass> is '12345678'.
	Example: cs hotspot start "My Hotspot" pass-123

_EOF
}

cmd_hotspot() {
    [[ $1 != 'start' && $1 != 'stop' ]] \
	&& echo -e "Usage:\n$(cmd_hotspot_help)" && exit 1

    $APP_DIR/misc/hotspot.sh "$@"
}
