cmd_create_help() {
    cat <<_EOF
    create
        Create the container '$CONTAINER'.

_EOF
}

rename_function cmd_create orig_cmd_create
cmd_create() {
    orig_cmd_create "$@"
    _create_cmd_guac
}

_create_cmd_guac() {
    local guacdir=$(basename $(pwd))
    mkdir -p $CSDIR/cmd/
    cat <<-__EOF__ > $CSDIR/cmd/guac.sh
cmd_guac() {
    case \$1 in
        url)   echo "https://$DOMAIN" ;;
        *)     cs @$guacdir guac "\$@" ;;
    esac
}
__EOF__
}
