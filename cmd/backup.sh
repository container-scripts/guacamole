cmd_backup_help() {
    cat <<_EOF
    backup
        Make a backup.

_EOF
}

cmd_backup() {
    # create the backup dir
    local backup="backup-$(date +%Y%m%d)"
    rm -rf $backup
    rm -f $backup.tgz
    mkdir $backup

    # dump the content of the database
    if [[ -z $DBHOST ]]; then
        local dump="mysqldump --allow-keywords --opt"
        cs exec $dump guacamole_db > $backup/guacamole_db.sql
    else
        local dump="mysqldump -h $DBHOST -P $DBPORT -u $DBUSER --password=$DBPASS --allow-keywords --opt"
        cs exec $dump $DBNAME > $backup/$DBNAME.sql
    fi

    # backup settings
    cp settings.sh $backup/

    # make the backup archive
    tar --create --gzip --preserve-permissions --file=$backup.tgz $backup/
    rm -rf $backup/
}
