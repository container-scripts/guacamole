cmd_config_help() {
    cat <<_EOF
    config
        Run configuration scripts inside the container.

_EOF
}

fail() { echo "$@" >&2; exit 1; }

cmd_config() {
    # check settings
    [[ $PASS == 'pass' ]] \
	&& fail "Error: PASS on 'settings.sh' has to be changed for security reasons."

    cs inject ubuntu-fixes.sh
    cs inject ssmtp.sh
    cs inject logwatch.sh
    cs inject apache2-redirect-to-https.sh

    cs inject fix-mariadb.sh

    # create the mariadb database
    [[ -n $DBHOST ]] && cs mariadb create

    # setup guacamole
    cs inject setup-guacamole.sh
    cs inject setup-apache2.sh
    #cs inject ram-optimizations.sh

    # install simple-chat
    podman cp -a $APP_DIR/misc/chat/ $CONTAINER:/var/www/html/
    cs exec chown www-data:www-data /var/www/html/chat/chat.txt
}
