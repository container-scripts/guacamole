APP="guacamole"

### Container settings.
IMAGE="conscr/guacamole"
CONTAINER="guacamole"
DOMAIN="guac.example.org"

### Email for getting a letsencrypt SSL certificate.
SSL_CERT_EMAIL=admin@example.org

### If not behind 'wsproxy', expose the port 443.
### In this case the DOMAIN and SSL_CERT_EMAIL should be commented
### because they are related to 'wsproxy'.
#PORTS="443:443"

############################################################################

### Guacamole admin user.
### For security reasons change at least the PASS.
ADMIN="admin"
PASS="pass"

### Default maximum connections for each server.
#MAX_CONNECTIONS="50"

############################################################################

### Uncomment DB settings to use a MabiaDB database. Change DBHOST if needed.
### You must have a server running MariaDB, for example install 'mariadb' container
### with container-scripts: https://gitlab.com/container-scripts/mariadb
#DBHOST=mariadb    # this is the name of the mariadb container
#DBPORT=3306
#DBNAME=guac_example_org
#DBUSER=guac_example_org
#DBPASS=123456

############################################################################

### SMTP server for sending notifications. You can build an SMTP server
### as described here:
### https://gitlab.com/container-scripts/postfix/blob/master/INSTALL.md
### Comment out if you don't have a SMTP server and want to use
### a gmail account (as described below).
#SMTP_SERVER="smtp.example.org"
#SMTP_DOMAIN="example.org"

### Gmail account for notifications. This will be used by ssmtp.
### You need to create an application specific password for your account:
### https://www.lifewire.com/get-a-password-to-access-gmail-by-pop-imap-2-1171882
#GMAIL_ADDRESS=username@gmail.org
#GMAIL_PASSWD=hdfhfdjkfglk
